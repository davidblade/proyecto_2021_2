/*!
* Start Bootstrap - Simple Sidebar v6.0.3 (https://startbootstrap.com/template/simple-sidebar)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-simple-sidebar/blob/master/LICENSE)
*/
// 
// Scripts
// 

window.addEventListener('DOMContentLoaded', event => {
    const sidebarToggle = document.body.querySelector('#sidebarToggle');//recuperando boton que tiene el id sidebarToggle
    if (sidebarToggle) {
        sidebarToggle.addEventListener('click', event => {
            console.log('click')
            event.preventDefault();
            document.body.classList.toggle('sb-sidenav-toggled');//colocamos la clase sb-sidenav-toggled si no tiene y si la tiene la quitamos eso hace el metodo toggle
            localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));//manejamos la variable sb|sidebar-toggle en localstorage del navegador
        });
    }

});
