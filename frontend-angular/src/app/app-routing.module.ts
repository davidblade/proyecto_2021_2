import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SkeletonComponent } from '@layout/skeleton/skeleton.component';
import { SkeletonDashboardComponent } from '@modules/dashboard/skeleton-dashboard/skeleton-dashboard.component';

const routes: Routes = [
  {
    path:'',//ruta directa  -> / -> raiz
    component:SkeletonComponent,
    children:[
      {
        path:'', // pasa a la primera ruta que este en las rutas hijas
        loadChildren:()=>//importar primero el modulo para poder llamarlo
          import('@modules/home/home.module').then((m)=>m.HomeModule)
      },
      {
        path:'auth',
        loadChildren:()=>//importar primero el modulo para poder llamarlo
          import('@modules/auth/auth.module').then((m)=>m.AuthModule)
      }
    ]
  },
  {
    path:'dashboard',
    component:SkeletonDashboardComponent,
    children:[
      {
        path:'home',
        loadChildren:()=>
          import('@modules/dashboard/dashboard.module').then((m)=>m.DashboardModule)
      },
      {
        path:'publications',
        loadChildren:()=>
          import('@modules/publications/publications.module').then((m)=>m.PublicationsModule)
      }
    ]
  }
];
//{useHash:true} -> quita el # de la url
@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
