import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-skeleton-dashboard',
  templateUrl: './skeleton-dashboard.component.html',
  styleUrls: ['./skeleton-dashboard.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class SkeletonDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
  }
}
