import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPublicationsComponent } from './list-publications/list-publications.component';
import { CreatePublicationsComponent } from './create-publications/create-publications.component';
import { SharedModule } from '@shared/shared.module';
import {PulicationsRoutingModule} from './publications-routing.module'


@NgModule({
  declarations: [
    ListPublicationsComponent,
    CreatePublicationsComponent
  ],
  imports: [
    SharedModule,
    PulicationsRoutingModule
  ]
})
export class PublicationsModule { }
