import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatePublicationsComponent } from './create-publications/create-publications.component';
import { ListPublicationsComponent } from './list-publications/list-publications.component';



const routes: Routes = [
    {
        path:'create-publications',
        component:CreatePublicationsComponent
    },
    {
        path:'list-publications',
        component:ListPublicationsComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PulicationsRoutingModule { }
