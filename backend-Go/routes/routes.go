package routes

import (
	controller "restapi/controllers"

	"github.com/gorilla/mux"
)

func Setup() *mux.Router {
	route := mux.NewRouter().StrictSlash(true)
	route.HandleFunc("/api/index", controller.Index)
	route.HandleFunc("/api/create-task", controller.CreateTask).Methods("POST")
	route.HandleFunc("/api/register", controller.Register).Methods("POST")
	return route
}
