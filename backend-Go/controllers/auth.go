package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"restapi/database"
	"restapi/models"

	"golang.org/x/crypto/bcrypt"
)

func Register(w http.ResponseWriter, r *http.Request) {
	var newUser models.User
	fmt.Println("into!!!")
	err := json.NewDecoder(r.Body).Decode(&newUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	//out, err := json.Marshal(newUser)
	//w.Header().Set("Content-type", "application/jsons")
	//w.Write(out)
	password, errpass := bcrypt.GenerateFromPassword([]byte(newUser.Password), bcrypt.DefaultCost)
	if errpass != nil {
		fmt.Fprintf(w, "Erro de encriptado %e", errpass)
	}

	collection := database.UserCollection
	ctx := database.Ctx
	newUser.Password = string(password)
	insert, insertError := collection.InsertOne(ctx, newUser)

	if insertError != nil {
		log.Fatal(insertError)
	}
	fmt.Println("nuevo usuario", insert.InsertedID)

}
