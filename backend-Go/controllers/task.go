package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"restapi/database"
	"restapi/models"
	"time"
)

func CreateTask(w http.ResponseWriter, r *http.Request) {
	var newTask models.Task
	err := json.NewDecoder(r.Body).Decode(&newTask)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	newTask.CreatedAt = time.Now()
	newTask.UpdateAt = time.Now()

	out, err := json.Marshal(newTask)
	w.Header().Set("Content-type", "application/jsons")
	w.Write(out)

	collection := database.TaskCollection
	ctx := database.Ctx

	insert, errInsert := collection.InsertOne(ctx, newTask)

	if errInsert != nil {
		log.Fatal(errInsert)
	}

	fmt.Println("se inserto el id", insert.InsertedID)

}
