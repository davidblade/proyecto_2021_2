package database

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var TaskCollection *mongo.Collection
var UserCollection *mongo.Collection
var Ctx = context.TODO()

func Connect() {
	client, err := mongo.Connect(Ctx, options.Client().ApplyURI("mongodb://user:pass@localhost:27017/sis414_2021"))
	if err != nil {
		log.Fatal(err)
	}
	err = client.Ping(Ctx, nil)
	if err != nil {
		log.Fatal(err)
	}
	TaskCollection = client.Database("sis414_2021").Collection("tasks")
	UserCollection = client.Database("sis414_2021").Collection("users")
	fmt.Println("conexion exitosa a la base de datos!!!")
}
