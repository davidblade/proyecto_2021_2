package models

import (
	"time"
)

type Task struct {
	Text      string    `bson:"text`
	CreatedAt time.Time `bson:"created_at"`
	UpdateAt  time.Time `bson:"updated_at"`
}
