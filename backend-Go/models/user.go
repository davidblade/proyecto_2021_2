package models

type User struct {
	Name     string
	Email    string `gorm:unique`
	Password string
}
