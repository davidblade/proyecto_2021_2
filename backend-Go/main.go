package main

import (
	"log"
	"net/http"
	"restapi/database"
	"restapi/routes"
)

func main() {
	database.Connect()
	router := routes.Setup()
	server := http.ListenAndServe(":3300", router)
	log.Fatal(server)
}
